import React, { useState } from 'react'
import './Achivement.css'
import user from '../assets/user.svg'
import school from '../assets/school.svg'
import school1 from '../assets/school1.svg'
import star from '../assets/star.svg'
import rectangle from '../assets/Path.svg'



const Achivement = () => {

    const [active, setactive] = useState(['active', 'inactive', 'inactive'])
    const [activeArray, setactiveArray] = useState([
        {
            img: 'https://images.unsplash.com/photo-1592219907299-b8084c39d625?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=80',
            data: "Pranay Dimble 3rd year Civil Engineering, FIT student ranked 6th at Mr. World (Junior)Competition held in Poland. He also got placed in the top 10 at Mr. Universe (Junior )Competition held in Hamburg, Germany in 2019."
        },
        {
            img: 'https://images.unsplash.com/photo-1588615419966-0c0f3bb797b3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60',
            data: 'Miss. Rupali Lone, alumni FIT, was selected as Junior Scientist at ISRO.'
        },
        {
            img: 'https://images.unsplash.com/photo-1592210566091-9e18a5fc01b4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60',
            data: 'Mr. Kushal Karpe alumni FIT, was selected amongst 800 students across India for Masters Course in Automation Design at IIM Mumbai and NID Ahmedabad.'
        },
    ])


    const [data] = useState([
        [
            {
                img: 'https://images.unsplash.com/photo-1592219907299-b8084c39d625?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=80',
                data: "Pranay Dimble 3rd year Civil Engineering, FIT student ranked 6th at Mr. World (Junior)Competition held in Poland. He also got placed in the top 10 at Mr. Universe (Junior )Competition held in Hamburg, Germany in 2019."
            },
            {
                img: 'https://images.unsplash.com/photo-1588615419966-0c0f3bb797b3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60',
                data: 'Miss. Rupali Lone, alumni FIT, was selected as Junior Scientist at ISRO.'
            },
            {
                img: 'https://images.unsplash.com/photo-1592210566091-9e18a5fc01b4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60',
                data: 'Mr. Kushal Karpe alumni FIT, was selected amongst 800 students across India for Masters Course in Automation Design at IIM Mumbai and NID Ahmedabad.'
            },
        ],
        [
            {
                img: 'https://images.unsplash.com/photo-1592219907299-b8084c39d625?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=80',
                data: 'Rahul Padalkar, Science teacher, Flora Valley School and Junior College, selected for the ‘Seed the Future Entrepreneurs’ competition organized by La Foundation Dassault Systems.'
            }
        ], [
            {
                img: 'https://images.unsplash.com/photo-1592219907299-b8084c39d625?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=80',
                data: 'Flora Valley School and Junior College selected among the top 10 schools for the ‘Seed the Future Entrepreneurs’ competition 2020-21 organized by La Foundation Dassault Systems.'
            },
            {
                img: 'https://images.unsplash.com/photo-1588615419966-0c0f3bb797b3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60',
                data: 'Flora College of Architecture successfully organized the maiden Annual Exhibition held at Yashwantrao Chavan Art Gallery, Pune on 3rd March 2020. '
            },
        ]
    ])

    const [activeImage, setactiveImage] = useState("https://images.unsplash.com/photo-1592219907299-b8084c39d625?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60")


    const onActiveChange = (index) => {
        let filterState = active.map((data, i) => {
            if (i === index) {
                return data = 'active'
            } else {
                return data = 'inactive'
            }
        })

        let clickedArrayIndex = data[index]
        setactiveImage(clickedArrayIndex[0].img)
        setactive(filterState)
        setactiveArray(clickedArrayIndex)
    }

    const changeImage=(url)=>{
        setactiveImage(url)
    }

    return (
        <section className="section4" id="achivements">
            <div className="head">
                <img src={rectangle} alt="rect" >
                </img>
                <div>ACHIVEMENTS</div>
            </div>
            <div className="container">
                <div className="container2">
                    <div className="header-achivements">
                        <div className="achivements" onClick={() => onActiveChange(0)}>
                            <div className="logo">
                                <img src={user} alt="user" />
                            </div>
                            <strong>Student Achivements</strong>
                            <div className={active[0]} />
                        </div>
                        <div className="achivements" onClick={() => onActiveChange(1)}>
                            <div className="logo">
                                <img src={school} alt="user" />
                            </div>
                            <strong>Faculty Achivements</strong>
                            <div className={active[1]} />
                        </div>
                        <div className="achivements" onClick={() => onActiveChange(2)}>
                            <div className="logo">
                                <img src={school1} alt="user" />
                            </div>
                            <strong>Department Achivements</strong>
                            <div className={active[2]} />
                        </div>
                    </div>
                </div>
                <div className="container4">
                    <div className="container3">
                        <div className="content1">
                            {
                                activeArray.map(data => {
                                    return (
                                        <div key={data.img} onClick={()=>changeImage(data.img)}>
                                            <img src={star} alt="star" />
                                            <span>{data.data}</span>
                                        </div>
                                    )
                                })
                            }

                        </div>
                        <div className="content2">
                            <div className="tropy" style={{backgroundImage:`url(${activeImage})`}}> 

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default React.memo(Achivement)
