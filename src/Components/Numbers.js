import React from 'react'
import './Numbers.css'

const Numbers = () => {
    return (
        <section className="section8">
            <div className="numbers">
                <div>
                    <span className="num">1200+</span>
                </div>
                <div>
                    <span className="num">2.7 Million</span>
                </div>
                <div>
                    <span className="num">58 Million</span>
                </div>
                <div>
                    <span className="num">800+</span>
                </div>
                <div>
                    <span className="num">200+</span>
                </div>
            </div>
            <div className="text">
               <div>
               <span className="content">Students Passed Out</span>
               </div>
               <div>
               <span className="content">Subscription of e-books books</span>
               </div> 
               <div>
               <span className="content">Subscription of e-journal articles</span>
               </div>
                <div>
                <span className="content">Students successfully placed</span>
               </div> 
               <div>
               <span className="content">Hours on soft skills</span>
               </div>

            </div>
        </section>
    )
}

export default React.memo(Numbers)
