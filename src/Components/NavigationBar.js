import React, { useState } from 'react'
import './NavigationBar.css'
import image from '../assets/Building.jpg'
import {Link} from 'react-router-dom'



const NavigationBar = ({ slide }) => {

    const [images] = useState([image])
 

    return (

        <header id="home">
            <nav>
            <div className="menu1" onClick={() => slide()}>
                    <i className="fa fa-bars"></i>
                </div>
    
                <Link to="/" className="logo">
                    <div className="logo-image"></div>
                    <div className="content">
                        <div className="black-text">FLORA INSTITUTES</div>
                    </div>
                </Link>
                <ul>
                    <li><div className="navbar">
                        <div className="dropdown">
                            <button className="dropbtn">ABOUT</button>
                            <div className="dropdown-content">
                                <Link to="/establishment">Establishment</Link>
                                <Link to="/founder">About Founder</Link>
                                <Link to="/co-founder">About Co-Founder</Link>
                                <Link to="/Executive">Executive Committee</Link>
                                <Link to="/Vision">Vision & Mission</Link>
                            </div>
                        </div>
                    </div></li>
                    <li><div className="navbar">
                        <div className="dropdown">
                            <button className="dropbtn">ADMISSION</button>
                            <div className="dropdown-content">
                                <a href="https://architecture.flora.ac.in/" target="_blank">Flora College of Architecture</a>
                                <a href="https://school.flora.ac.in/index.html" target="_blank">Flora Valley School and Junior College</a>
                                <a href="https://flora.ac.in/" target="_blank">Flora Institute of Technology</a>
                            </div>
                        </div>
                    </div></li>
                    <li><Link to="/admission">CONTACT</Link></li>
                </ul>
            </nav>
            <div className="header-image" style={{ backgroundImage: `url(${images}` }}>
                <div className="content">
                    <div className="first-block">RANKED AMONGEST</div>
                    <div className="second-block">TOP COLLAGES IN MAHARASHTRA</div>
                    <div className="third-block">FOR ACADEMIC EXCLLENCE</div>
                </div>
                <div className="content2">
                    <div className="first-block">Admission open for B.E 2020-21</div>
                    <a href="#admission
                    " className="second-block">ENQUIRY NOW</a>
                </div>
            </div>
        </header>
    )
}

export default React.memo(NavigationBar)
